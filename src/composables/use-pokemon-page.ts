import {ref} from 'vue'
import PokeListItem from '../models/PokeListItem'
import axios, {AxiosResponse} from 'axios'
import PokeList from '../models/PokeList'

const initialPageUrl = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=20'
const pokemonList = ref<PokeListItem[]>()
const nextPage = ref<string>()
const previousPage = ref<string>()

export default function usePokemonPage() {
  function setValues(fetchedPokeList: AxiosResponse<PokeList>) {
    pokemonList.value = fetchedPokeList.data.results.map(item => {
      const urlArray = item.url.split('/')
      const id = urlArray[urlArray.length - 2]
      item.number = parseInt(id)
      return item
    })

    nextPage.value = fetchedPokeList.data.next
    previousPage.value = fetchedPokeList.data.previous
  }

  const initPokemonList = async () => {
    const fetchedPokeList: AxiosResponse<PokeList> = await axios.get(
      initialPageUrl
    )
    setValues(fetchedPokeList)
  }

  const fetchNextPage = async () => {
    if (nextPage.value != null) {
      const fetchedPokeList: AxiosResponse<PokeList> = await axios.get(
        `${nextPage.value}`
      )
      setValues(fetchedPokeList)
    }
  }

  const fetchPrevPage = async () => {
    if (previousPage.value != null) {
      const fetchedPokeList: AxiosResponse<PokeList> = await axios.get(
        `${previousPage.value}`
      )
      setValues(fetchedPokeList)
    }
  }

  return {
    pokemonList,
    fetchNextPage,
    fetchPrevPage,
    initPokemonList,
  }
}
