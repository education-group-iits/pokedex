import {ref} from 'vue'
import PokeListItem from '../models/PokeListItem'
import {Pokemon} from '../models/Pokemon'
import axios, {AxiosResponse} from 'axios'

const pokemon = ref<Pokemon>()

export default function usePokemonSelection() {
  const setPokemonSelection = async function(pokemonListItem: PokeListItem) {
    const fetchedPokemon: AxiosResponse<Pokemon> = await axios.get(
      `${pokemonListItem.url}`
    )

    pokemon.value = fetchedPokemon.data
  }

  return {pokemon, setPokemonSelection}
}
