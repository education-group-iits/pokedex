import PokeListItem from './PokeListItem'

export default interface PokeList {
  count: number
  next: string
  previous: string
  results: PokeListItem[]
}
