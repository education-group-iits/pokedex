export default interface PokeListItem {
  name: string
  number: number
  url: string
}
